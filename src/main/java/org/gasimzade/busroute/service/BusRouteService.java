package org.gasimzade.busroute.service;

import org.gasimzade.busroute.domain.BusRoute;

/**
 * Created by pgasimzade on 15/5/2017.
 */
public interface BusRouteService {

    BusRoute checkDirectBusRoute(Integer departureSid, Integer arrivalSid);
}
