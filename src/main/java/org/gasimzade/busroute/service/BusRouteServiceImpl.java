package org.gasimzade.busroute.service;

import org.gasimzade.busroute.common.ErrorCode;
import org.gasimzade.busroute.common.BusRouteGraph;
import org.gasimzade.busroute.domain.BusRoute;
import org.gasimzade.busroute.exception.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by pgasimzade on 15/5/2017.
 */
@Service
public class BusRouteServiceImpl implements BusRouteService {

    @Autowired
    private BusRouteGraph busRouteGraph;

    @Override
    public BusRoute checkDirectBusRoute(Integer departureSid, Integer arrivalSid) {
        if (departureSid == null || arrivalSid == null) {
            throw new ValidationException(ErrorCode.VALIDATION_MISSING_REQUEST_PARAMS);
        }

        BusRoute busRoute = new BusRoute();
        busRoute.setDepartureSid(departureSid);
        busRoute.setArrivalSid(arrivalSid);
        busRoute.setDirectBusRoute(busRouteGraph.isDirectBusRouteExist(departureSid,arrivalSid));

        return busRoute;
    }
}
