package org.gasimzade.busroute;

import org.gasimzade.busroute.common.FileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by pgasimzade on 15/5/2017.
 */
@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private FileReader fileReader;

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        if (args.length == 0) {
            logger.error("ERROR : Argument cannot be empty!");
            System.exit(1);
        }

        String fileName = args[0];
        fileReader.readFile(fileName);
    }
}
