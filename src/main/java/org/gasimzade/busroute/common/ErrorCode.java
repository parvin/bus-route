package org.gasimzade.busroute.common;

/**
 * Created by pgasimzade on 15/5/2017.
 */
public enum ErrorCode {
    VALIDATION(1000, "Validation error"),
    VALIDATION_MISSING_REQUEST_PARAMS(1001, "Empty request params"),

    UNEXPECTED_ERROR(9999, "Internal API Error");

    private Integer code;
    private String message;

    ErrorCode(Integer code, String message){
        this.code = code;
        this.message = message;
    }

    public Integer getCode(){
        return this.code;
    }

    public String getMessage(){
        return this.message;
    }
}
