package org.gasimzade.busroute.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This class is used to parse file and create Graph using BusRouteGraph class
 *
 * Created by pgasimzade on 15/5/2017.
 */
@Component
public class FileReader {

    @Autowired
    private BusRouteGraph busRouteGraph;

    public Map<Integer, List<Integer>> readFile(String fileName) {
        Map<Integer, List<Integer>> map = new HashMap<>();

        if (!StringUtils.isEmpty(fileName)) {
            try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
                List<String> busRoutes = stream.skip(1).collect(Collectors.toList());

                for(String line : busRoutes) {
                    String[] sids = line.split(" ");
                    for(int i = 1; i < sids.length -1; i++ ) {
                        busRouteGraph.createPath(Integer.parseInt(sids[i]), Integer.parseInt(sids[i+1]));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return map;
    }
}
