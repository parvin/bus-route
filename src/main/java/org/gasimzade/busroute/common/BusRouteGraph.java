package org.gasimzade.busroute.common;

import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Graph implementation which holds the list of adjacent stations
 *
 * Created by pgasimzade on 15/5/2017.
 */
@Component
public class BusRouteGraph {

    private static final Integer MAX_STATION = 1000000;

    private List<LinkedList<Integer>> adjacencyList;

    public BusRouteGraph () {
        adjacencyList = Stream.generate(LinkedList<Integer>::new).limit(MAX_STATION).collect(Collectors.toList());
    }

    public void createPath(Integer departureSid, Integer arrivalSid) {
        List<Integer> subList = adjacencyList.get(departureSid);
        if (subList == null) {
            subList = new LinkedList<>();
        }

        subList.add(arrivalSid);
    }

    public boolean isDirectBusRouteExist(Integer departureSid, Integer arrivalSid) {
        List<Boolean> visited = new ArrayList<>(Arrays.asList(new Boolean[MAX_STATION]));
        Collections.fill(visited, Boolean.FALSE);

        LinkedList<Integer> queue = new LinkedList<>();

        visited.add(departureSid, Boolean.TRUE);
        queue.add(departureSid);

        while (queue.size()!=0) {
            departureSid = queue.poll();

            for(Integer currentSid : adjacencyList.get(departureSid)) {
                if (currentSid == arrivalSid) {
                    return true;
                }

                if (visited.get(currentSid) != null && !visited.get(currentSid)) {
                    visited.add(currentSid, Boolean.TRUE);
                    queue.add(currentSid);
                }
            }
        }

        return false;
    }
}
