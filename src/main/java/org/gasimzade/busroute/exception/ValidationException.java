package org.gasimzade.busroute.exception;

import org.gasimzade.busroute.common.ErrorCode;

/**
 * Created by pgasimzade on 15/5/2017.
 */
public class ValidationException extends RuntimeException {
    private Integer code;
    private String message;

    public ValidationException() {
        super(ErrorCode.VALIDATION.getCode()+"-"+ErrorCode.VALIDATION.getMessage());
        this.code = ErrorCode.VALIDATION.getCode();
        this.message = ErrorCode.VALIDATION.getMessage();
    }

    public ValidationException(ErrorCode errorCode){
        super(errorCode.getCode() + "-" + errorCode.getMessage());
        this.code = errorCode.getCode();
        this.message = errorCode.getMessage();
    }

    public ValidationException(Integer code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public Integer getErrorCode() {
        return code;
    }

    public void setErrorCode(Integer code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return message;
    }

    public void setErrorMessage(String message) {
        this.message = message;
    }
}
