package org.gasimzade.busroute.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by pgasimzade on 15/5/2017.
 */
public class BusRoute {

    @JsonProperty("dep_sid")
    private Integer departureSid;

    @JsonProperty("arr_sid")
    private Integer arrivalSid;

    @JsonProperty("direct_bus_route")
    private Boolean directBusRoute;

    public Integer getDepartureSid() {
        return departureSid;
    }

    public void setDepartureSid(Integer departureSid) {
        this.departureSid = departureSid;
    }

    public Integer getArrivalSid() {
        return arrivalSid;
    }

    public void setArrivalSid(Integer arrivalSid) {
        this.arrivalSid = arrivalSid;
    }

    public Boolean getDirectBusRoute() {
        return directBusRoute;
    }

    public void setDirectBusRoute(Boolean directBusRoute) {
        this.directBusRoute = directBusRoute;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BusRoute)) return false;

        BusRoute busRoute = (BusRoute) o;

        if (departureSid != null ? !departureSid.equals(busRoute.departureSid) : busRoute.departureSid != null)
            return false;
        if (arrivalSid != null ? !arrivalSid.equals(busRoute.arrivalSid) : busRoute.arrivalSid != null) return false;
        return directBusRoute != null ? directBusRoute.equals(busRoute.directBusRoute) : busRoute.directBusRoute == null;

    }

    @Override
    public int hashCode() {
        int result = departureSid != null ? departureSid.hashCode() : 0;
        result = 31 * result + (arrivalSid != null ? arrivalSid.hashCode() : 0);
        result = 31 * result + (directBusRoute != null ? directBusRoute.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BusRoute{");
        sb.append("departureSid=").append(departureSid);
        sb.append(", arrivalSid=").append(arrivalSid);
        sb.append(", directBusRoute=").append(directBusRoute);
        sb.append('}');
        return sb.toString();
    }
}
