package org.gasimzade.busroute.controller;

import org.gasimzade.busroute.domain.BusRoute;
import org.gasimzade.busroute.service.BusRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST API to check bus routes between given stations
 *
 * Created by pgasimzade on 15/5/2017.
 */
@RestController
@RequestMapping("/api")
public class BusRouteController {

    @Autowired
    private BusRouteService busRouteService;

    /**
     * Checks whether there exist a direct bus route between given station IDs
     *
     * @param departureSid
     * @param arrivalSid
     * @return JsonObject with the following structure will be returned.
     *
     * {
     *     "type": "object",
     *     "properties": {
     *          "dep_sid": {
     *              "type": "integer"
     *          },
     *          "arr_sid": {
     *              "type": "integer"
     *          },
     *          "direct_bus_route": {
     *              "type": "boolean"
     *          }
     *      },
     *      "required": [
     *          "dep_sid",
     *          "arr_sid",
     *          "direct_bus_route"
     *      ]
     * }
     */
    @GetMapping("/direct")
    public BusRoute checkDirectBusRoute(@RequestParam(value = "dep_sid") Integer departureSid,
                                        @RequestParam(value = "arr_sid") Integer arrivalSid){
        return busRouteService.checkDirectBusRoute(departureSid,arrivalSid);
    }
}
